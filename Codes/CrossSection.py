#This is a snippet of code to show how to do cross sections:
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

#First make List of points and fill list with values we want to plot:
 NLons=100
NRads=20
Lons=np.linspace(-180,180,NLons,endpoint=True)      #Longitudes of cross section
Lats=[0]                                            #Latitudes of cross Section
Rads=np.linspace(0,ICRadius,NRads,endpoint=True)    #Radius of Cross Section
LocsCart=[]
LocsSph=[]
for lon in Lons:
    for lat in Lats:
            for rad in Rads:
                x,y,z=SphericalToCart2(lon,lat,rad)
                LocsSph.append([lon,lat,rad])
                LocsCart.append([x,y,z])


gs = gridspec.GridSpec(1,1)
figure=plt.figure(figsize=(10,8)) 
plt.suptitle(Title,fontsize=20)
norm = cm.colors.Normalize(vmin=vmin, vmax=vmax)
#EQUATORIAL
ax=plt.subplot(gs[0,0],projection='polar')
if contour:
    cnt=ax.tricontourf(EQ_theta, EQ_r, EQ_data,levels=300,cmap=cmap,vmin=vmin,vmax=vmax)
    for c in cnt.collections:
        c.set_edgecolor("face")
else:
    EQ_data=np.array(EQ_data).reshape(NLons,NRads)
    EQ_theta=np.array(EQ_theta).reshape(NLons,NRads)
    EQ_r=np.array(EQ_r).reshape(NLons,NRads)
    ax.pcolormesh(EQ_theta,EQ_r,EQ_data,cmap=cmap,vmin=vmin,vmax=vmax,shading="auto")
ax.set_title('Equatorial',color='tab:blue',y=1.08,fontsize=15)
ax.set_rticks([600])  # less radial ticks
ax.set_xticks(np.pi/180. * np.linspace(180,  -180, 4, endpoint=False))
ax.set_thetalim(-pi,pi)
ax.set_theta_zero_location("S")