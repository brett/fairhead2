############################################################################################
#####################################   Geometry   #########################################
############################################################################################
# Henry Brett, Utrecht University, 2020, h.brett@uu.nl
# This is a library of functions to convert between spherical and cartesian co-ordinates and in as an
# effecient a way as possible calculate certain parameters such as distances between points
# Locations and vectors are always defined in the same way: [x,y,z] or [lon,lat,rad] in spherical coords
# (some older functions still use [lon,lat,depth] !be careful!)

from math import pi,cos,sin,acos,asin,sqrt,radians,atan
import numpy as np

#region-----------Define Global Variables--------------------------------------#
global ICDepth,Surface,ICRadius 
ICDepth=5153.5
Surface=6371
ICRadius=Surface-ICDepth
#endregion-----------Define Global Variables--------------------------------------#

#------------GEOMETRY FUNCTIONS-----------#
def Vector(x,y,z):
    vec=[x,y,z]
    return vec
 
def LinePlaneIntersection(plane_normal,plane_point,line_direction,line_point):
    epsilon=10**-6         #tolerance
    #dot product
    ndotu = plane_normal.dot(line_direction) 
    if ndotu ==0 :
        #print('ERROR: RP IS PARRALEL TO PLANE. CANNOT CALCULATE IP.')
        IP=[10000,10000,10000]
    else:
        w = line_point - plane_point
        si = -plane_normal.dot(w) / ndotu
        IP = w + si * line_direction+ plane_point #point of intersection
    #output:
    return IP

def AngleBetweenTwoLines(Vector1,Vector2):
    Num=Vector1[0]*Vector2[0]+Vector1[1]*Vector2[1]+Vector1[2]*Vector2[2]
    Den=sqrt(Vector1[0]**2+Vector1[1]**2+Vector1[2]**2)*sqrt(Vector2[0]**2+Vector2[1]**2+Vector2[2]**2)
    test=Num/Den
    if abs(round(test,6)) !=1:
        angle=acos(Num/Den)*(180/pi)
    else:
        angle=0
    if angle >90:
        angle=90-(angle-90)
    return angle

def PointRadius(Vector):
    #Given a point what is the distance from Earth's centre?
    radius=sqrt(Vector[0]**2+Vector[1]**2+Vector[2]**2)
    return radius

def DirectionVector(Vector1,Vector2):
    Direction_Vector=np.array([Vector2[0]-Vector1[0],Vector2[1]-Vector1[1],Vector2[2]-Vector1[2]])
    return Direction_Vector

def SphericalToCart(Lon,Lat,depth):
    r=(Surface-depth)
    co_lat=90-Lat

    if hasattr(Lon, "__len__"):        
        theta=co_lat*np.pi/180
        sigma=Lon*np.pi/180
        xs=r*np.sin(theta)*np.cos(sigma)
        ys=r*np.sin(theta)*np.sin(sigma)
        zs=r*np.cos(theta)
    else:
        theta=co_lat*pi/180
        sigma=Lon*pi/180
        xs=r*sin(theta)*cos(sigma)
        ys=r*sin(theta)*sin(sigma)
        zs=r*cos(theta)        
    return xs,ys,zs

def SphericalToCart_lim(Lon,Lat,depth):
    r=(Surface-depth)
    co_lat=90-Lat

    if hasattr(Lon, "__len__"):        
        theta=co_lat*np.pi/180
        sigma=Lon*np.pi/180
        xs=r*np.sin(theta)*np.cos(sigma)
        ys=r*np.sin(theta)*np.sin(sigma)
        zs=r*np.cos(theta)
    else:
        theta=co_lat*pi/180
        sigma=Lon*pi/180
        xs=r*sin(theta)*cos(sigma)
        ys=r*sin(theta)*sin(sigma)
        zs=r*cos(theta)        
    return round(xs,2),round(ys,2),round(zs,2)

def SphericalToCart2(Lon,Lat,radius):
    r=radius
    co_lat=90-Lat
    if hasattr(Lon, "__len__"):        
        theta=co_lat*np.pi/180
        sigma=Lon*np.pi/180
        xs=r*np.sin(theta)*np.cos(sigma)
        ys=r*np.sin(theta)*np.sin(sigma)
        zs=r*np.cos(theta)
    else:
        theta=co_lat*pi/180
        sigma=Lon*pi/180
        xs=r*sin(theta)*cos(sigma)
        ys=r*sin(theta)*sin(sigma)
        zs=r*cos(theta)        
    return xs,ys,zs

def SphericalToCart2_lim(Lon,Lat,radius):
    r=radius
    co_lat=90-Lat
    if hasattr(Lon, "__len__"):        
        theta=co_lat*np.pi/180
        sigma=Lon*np.pi/180
        xs=r*np.sin(theta)*np.cos(sigma)
        ys=r*np.sin(theta)*np.sin(sigma)
        zs=r*np.cos(theta)
    else:
        theta=co_lat*pi/180
        sigma=Lon*pi/180
        xs=r*sin(theta)*cos(sigma)
        ys=r*sin(theta)*sin(sigma)
        zs=r*cos(theta)        
    return round(xs,2),round(ys,2),round(zs,2)

def CartToSpherical(Vector):
    radius=sqrt(Vector[0]**2+Vector[1]**2+Vector[2]**2)
    co_lat=acos(Vector[2]/radius)*(180/pi)
    lat=90-co_lat
    if Vector[0] != 0:
        theta=abs(atan(Vector[1]/Vector[0])*(180/pi))
        if (Vector[0] >0) and (Vector[1] >= 0) :
            #sector 1 0-->90
            lon=theta
        if (Vector[0] <0) and (Vector[1] >= 0) :
            #Sector 2 90 --> 180
            lon=180-theta
        if (Vector[0] <0) and (Vector[1] < 0) :
            #Sector 3 180 --> -90
            lon=-180+theta
        if (Vector[0] >0) and (Vector[1] < 0) :
            #sector 4 -90 --> 0
            lon=-theta
    else:
        if Vector[0] == 0 and Vector[1] == 0 :
            lon=0 #centre
        if Vector[0] == 0 and Vector[1] < 0 :
            lon=-90
        if Vector[0] == 0 and Vector[1] > 0 :
            lon=90
    lon=round(lon,6)
    lat=round(lat,6)
    radius=round(radius,6)
    return lon,lat,radius

def CartToSpherical2(Vector,RadSeg):
    radius=sqrt(Vector[0]**2+Vector[1]**2+Vector[2]**2)
    co_lat=acos(Vector[2]/radius)*(180/pi)
    lat=90-co_lat
    if Vector[0] != 0:
        theta=abs(atan(Vector[1]/Vector[0])*(180/pi))
        if (Vector[0] >0) and (Vector[1] >= 0) :
            #sector 1 0-->90
            lon=theta
        if (Vector[0] <0) and (Vector[1] >= 0) :
            #Sector 2 90 --> 180
            lon=180-theta
        if (Vector[0] <0) and (Vector[1] < 0) :
            #Sector 3 180 --> -90
            lon=180+abs(theta)
        if (Vector[0] >0) and (Vector[1] < 0) :
            #sector 4 -90 --> 0
            lon=360-theta
    else:
        if Vector[0] == 0 and Vector[1] == 0 :
            lon=0 #centre
        if Vector[0] == 0 and Vector[1] < 0 :
            lon=270
        if Vector[0] == 0 and Vector[1] > 0 :
            lon=90
    lon=round(lon,6)
    #Now we have longitude between 0-360. lets correct for RadSeg;
    if RadSeg.lon2 > 360 and lon < RadSeg.lon1:
        lon=lon+360
    lat=round(lat,6)
    radius=round(radius,6)
    return lon,lat,radius

def PointLineDistance(x_1,x_2,x_0):
    #Calculate the distance between a point and a line.
    # x_1 and x_2 are vectors which define the in and out points of the Ray
    # x_0 is a vector which defines the point.
    num=np.cross((x_2-x_1),(x_1-x_0))
    num_dist=sqrt(num[0]**2+num[1]**2+num[2]**2)
    den=(x_2-x_1)
    den_dist=sqrt(den[0]**2+den[1]**2+den[2]**2)
    distance=num_dist/den_dist
    return distance

def PointLineDistance_coord(x_1,x_2,x_0):
    b=PointLineDistance(x_1,x_2,x_0)
    c=ProximityCalculator2(x_1,x_0)
    a=sqrt(c**2-b**2)
    RP_dist_total=ProximityCalculator2(x_1,x_2)
    RP_direction_vector=DirectionVector(x_1,x_2)
    x_3=x_1+RP_direction_vector*(a/RP_dist_total)
    return x_3

def ProximityCalculator(xs1,xs2,ys1,ys2,zs1,zs2):
    distance=np.sqrt((xs2-xs1)**2+(ys2-ys1)**2+(zs2-zs1)**2)
    return distance

def ProximityCalculator2(Vector1,Vector2):
    distance=np.sqrt((Vector2[0]-Vector1[0])**2+(Vector2[1]-Vector1[1])**2+(Vector2[2]-Vector1[2])**2)
    return distance

def RPIn3DTriangularPlane(Node1,Node2,Node3,RP,invec):
    '''This is to work out if a point exists on a 3D Triangle
        Methodology:
            1.Workout if intersection on plane
            2.Workout if bounded by triangle on that plane''' 
    Vec1=DirectionVector(Node1,Node2)
    Dist_temp=ProximityCalculator2(Node1,Node2)
    Node2_further=Node1+Normalize(Vec1)*Dist_temp*4
    Vec1=DirectionVector(Node1,Node2_further)

    Vec2=DirectionVector(Node1,Node3)
    Dist_temp=ProximityCalculator2(Node1,Node3)
    Node3_further=Node1+Normalize(Vec2)*Dist_temp*4
    Vec2=DirectionVector(Node1,Node3_further)


    Plane_normal=np.cross(Vec1,Vec2)
    IP=LinePlaneIntersection(Plane_normal,Node1,RP,invec)
    if ProximityCalculator2(IP,[0,0,0]) <=ICRadius: #If point on plane is inside Inner Core then work out if in triangle
        W=IP-Node1
        u=Vec1
        v=Vec2
        test1=(np.dot(u,v)**2-np.dot(u,u)*np.dot(v,v))
        if test1 != 0:
            S_i=(np.dot(u,v)*np.dot(W,v)-np.dot(v,v)*np.dot(W,u))/test1
            T_i=(np.dot(u,v)*np.dot(W,u)-np.dot(u,u)*np.dot(W,v))/test1
            if S_i>=0 and T_i>=0 and S_i+T_i<=1:
                Intersect_switch=True
            else:
                Intersect_switch=False
        else:
            Intersect_switch=False
    else:
        Intersect_switch=False
    return IP,Intersect_switch

def LocCalc(Start,Dist,Vec):
    return Start+Dist*Vec

def Normalize(Vector):
    Vector_norm_factor=sqrt(Vector[0]**2+Vector[1]**2+Vector[2]**2)
    if Vector_norm_factor == 0:
        Vector_norm =[0,0,0]
        print('Normalized Vector has magnitude 0')
    else:
        Vector_norm=Vector/Vector_norm_factor
    return Vector_norm

def LineSphereInteraction(Vector1,Loc1,SphereRadius):
    '''Calculate if Line of unit vector Vector1 located at Loc1 
    intersects sphere with origin 0,0,0 (ie. Earth's centre) with radius SphereRadius'''
    Dot_product=np.dot(Vector1,Loc1)
    Test=Dot_product**2-(ProximityCalculator2(Loc1,[0,0,0])**2-SphereRadius**2)
    if Test < 0:
        d=-20000    #Ie. no intersection
    if Test == 0:
        d=Dot_product
    if Test > 0:    
        d=[-Dot_product+sqrt(Test),-Dot_product-sqrt(Test)]
    return d

def HaversineFormula(loc1,loc2):
    lon1,lat1,rad1=CartToSpherical(loc1)
    lon2,lat2,rad2=CartToSpherical(loc2)
    lon1=lon1*(pi/180)
    lat1=lat1*(pi/180)
    lon2=lon2*(pi/180)
    lat2=lat2*(pi/180)
    A=sin((lat1-lat2)/2)**2
    B=cos(lat1)*cos(lat2)*(sin(lon2-lon1/2)**2)
    d=2*rad1*asin(sqrt(A+B))
    return d

def GCDist(lon1,lat1,lon2,lat2):
    lon1=lon1*(pi/180)
    lat1=lat1*(pi/180)
    lon2=lon2*(pi/180)
    lat2=lat2*(pi/180)
    lon_diff=abs(lon1-lon2)
    A=(cos(lat2)*sin(lon_diff))**2
    B=(cos(lat1)*sin(lat2)-sin(lat1)*cos(lat2)*cos(lon_diff))**2
    C=sin(lat1)*sin(lat2)+cos(lat1)*cos(lat2)*cos(lon_diff)
    Dist=atan(sqrt(A+B)/C)
    return Dist

def SphericalAngle(A,B,C):
    A=Normalize(np.array(A))
    B=Normalize(np.array(B))
    C=Normalize(np.array(C))

    u=np.cross(B,A)
    v=np.cross(A,C)
    angle=AngleBetweenTwoLines(u,v)*(pi/180)
    
    return angle

def SphericalAngles(A,B,C):
    '''A, B and C are points on a sphere in Cart Coords
    They make up a triangle. calculate the angles alpha,beta,gamma between them'''
    # Rad_test1=ProximityCalculator2(A,[0,0,0]) #Should be same for all three
    # Rad_test2=ProximityCalculator2(B,[0,0,0]) #Should be same for all three
    # Rad_test3=ProximityCalculator2(C,[0,0,0]) #Should be same for all three

    #Calculate Lengths A,B,C:
    a=acos(np.dot(Normalize(np.array(A)),Normalize(np.array(B))))
    b=acos(np.dot(Normalize(np.array(B)),Normalize(np.array(C))))
    c=acos(np.dot(Normalize(np.array(C)),Normalize(np.array(A))))
    alpha=acos((cos(a)-(cos(b)*cos(c)))/(sin(b)*sin(c)))        
    beta=acos((cos(b)-(cos(c)*cos(a)))/(sin(c)*sin(a)))    
    gamma=acos((cos(c)-(cos(a)*cos(b)))/(sin(a)*sin(b)))    

    return alpha,beta,gamma 

def SphericalDistance(loc1,loc2):
    # loc1=Normalize(np.array(loc1))
    # loc2=Normalize(np.array(loc2))
    # print(np.cross(loc1,loc2))
    # print(Normalize(np.cross(loc1,loc2)))
    dist=atan(ProximityCalculator2([0,0,0],np.cross(loc1,loc2))/np.dot(loc1,loc2))
    return dist
#------------GEOMETRY FUNCTIONS-----------#
