    
    def DLS_single(self,Data,DR,G_matrix):
        '''Run DLS on a model state and pick the best model per misfit'''
        G_t=G_matrix.transpose()
     
        Damping_ratio=np.identity(self.M_len)*DR
        M_0=np.zeros(self.M_len)
        Inverse_operator=np.linalg.inv(np.matmul(G_t,G_matrix)+Damping_ratio)
        M_new=np.matmul(np.matmul(Inverse_operator,G_t),Data.D_matrix)
        #Resolution Matrix
        Resolution=np.matmul(np.matmul(Inverse_operator,G_t),G_matrix)
        Ind_model_param=np.trace(Resolution)
        #Model Size
        Model_size=np.sum(M_new**2)
        #Misfit
        dt_pred=np.matmul(G_matrix,M_new)/Data.ICT
        Misfit=sqrt(np.sum((Data.D_matrix_norm-dt_pred)**2))
        return Misfit,M_new,dt_pred