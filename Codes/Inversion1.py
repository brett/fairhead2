##########################################################
# INVERSION: Attenuation Tomogrpahy
##########################################################

#Import functions
import numpy as np
import pickle
import sys,os,glob
from tqdm import tqdm

#Personal Functions
from Geometry_Library import *
from RP_Data_Model_Library import *

#Define Directories
CodesDirectory=sys.path[0]
TransFolder=os.path.dirname(CodesDirectory)
DataDirectory=TransFolder+'/Data'
ModelDirectory=TransFolder+'/Models'
FiguresDirectory=TransFolder+'/Figures'
RaypathObjects=DataDirectory+'/RaypathObjects'

#Read In Data:

#Create Model:

#Raytrace Raypaths Through Model:

#Predict Data: