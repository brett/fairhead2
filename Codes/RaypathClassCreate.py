# Henry Brett 2020
# This is a script to read in all earthquake info from data set, calculate hte AK135 raypaths and create a raypath object
# with name station_year_julday_time.obj


from obspy.taup import TauPyModel
import sys,os,glob
from math import pi,cos,sin,acos,asin,sqrt,radians,atan,atan2
import numpy as np
import pickle
import pandas as pd
import tqdm
from obspy.geodetics import locations2degrees,gps2dist_azimuth
import geographiclib
#Personal Functions
from Geometry_Library import *
from RP_Data_Model_Library import *

global ICDepth,Surface,ICRadius 
ICDepth=5153.5
Surface=6371
ICRadius=Surface-ICDepth
sphere=geographiclib.geodesic.Geodesic(Surface,0)   #Spherical Geographiclib geodesic object!

#Define Directories
CodesDirectory=sys.path[0]
TransFolder=os.path.dirname(CodesDirectory)
DataDirectory=TransFolder+'/Data'
ModelDirectory=TransFolder+'/Models'
FiguresDirectory=TransFolder+'/Figures'
RaypathObjects=DataDirectory+'/RaypathObjects'

#Functions
def AK135RaypathExtract(Evdepth,Distance,phase):
    #Calculate Arrival times from TauP
    model=TauPyModel(model="AK135")
    phases=[phase]
    arrivals= model.get_travel_times(source_depth_in_km=Evdepth,distance_in_degree=Distance,phase_list=phases)
    arrivals= model.get_pierce_points(Evdepth,Distance,phase_list=phases)    
    arrivals= model.get_ray_paths(source_depth_in_km=Evdepth, distance_in_degree=Distance, phase_list=phases)
    Raypath=arrivals[0].path
    return Raypath
    
def RayPathCartesianConvert(Raypath,Evlon,Evlat,Azimuth):
    Nodes_sph=[]
    Nodes=[]
    Nodes_dist=[]
    #Surface-->CMB
    for i in range(0,len(Raypath)):
        dep_q=Raypath[i][3]
        loc=sphere.ArcDirect(lat1=Evlat,lon1=Evlon,azi1=Azimuth,a12=Raypath[i][2]*(180/pi),outmask=1929)
        lat_q=loc['lat2']
        lon_q=loc['lon2']
        Nodes_sph.append([lon_q,lat_q,dep_q])
        x1,y1,z1=SphericalToCart_lim(lon_q,lat_q,dep_q)
        Nodes.append([x1,y1,z1])    
    return Nodes,Nodes_sph

def InnerCoreRayPathCartesianConvert(Raypath,Evlon,Evlat,Azimuth):
    Nodes_sph=[]
    Nodes=[]
    Nodes_dist=[]
    #Surface-->CMB
    for i in range(0,len(Raypath)):
        if dep_q >= 5153.5:
            dep_q=Raypath[i][3]
            loc=sphere.ArcDirect(lat1=Evlat,lon1=Evlon,azi1=Azimuth,a12=Raypath[i][2]*(180/pi),outmask=1929)
            lat_q=loc['lat2']
            lon_q=loc['lon2']
            Nodes_sph.append([lon_q,lat_q,dep_q])
            x1,y1,z1=SphericalToCart_lim(lon_q,lat_q,dep_q)
            Nodes.append([x1,y1,z1])    
    return Nodes,Nodes_sph

def ResamplePath(Nodes):
    Nodes_new=[]
    Nodes_new.append(Nodes[0])
    for i in range(0,len(Nodes)-1):
        Vector,Node1Node2Dist,CentreNode_loc=NodesGeometry(Nodes[i],Nodes[i+1])
        Nodes_new.append(CentreNode_loc)
        Nodes_new.append(Nodes[i+1])
    return Nodes_new

def ResamplePathSmart(Nodes,Nodes_dist,Dist_tol_abs):
    Nodes_new=[]
    Nodes_new.append(Nodes[0])
    for i in range(1,len(Nodes)):
        if Nodes_dist[i] > Dist_tol_abs:
            Vector,Node1Node2Dist,CentreNode_loc=NodesGeometry(Nodes[i-1],Nodes[i])
            CentreNode_loc=[round(CentreNode_loc[0],2),round(CentreNode_loc[1],2),round(CentreNode_loc[2],2)]
            Nodes_new.append(CentreNode_loc)
        Nodes_new.append(Nodes[i])
    return Nodes_new

def NodesGeometry(Node1,Node2):
    '''Take two nodes and collect important geom info
    Params:
        Vector - from N1 to N2
        CentreNode_loc - Location of point exactly betewen N1 and N2
        Vector_perp1 - perp to Vector in plane of N1,N2 and [0,0,0]
        Vector_perp2 - perp to Vector perp to plane of N1,N2 and [0,0,0]'''
    Vector=Normalize(DirectionVector(Node1,Node2))              #!Normalized vector
    Node1Node2Dist=ProximityCalculator2(Node1,Node2)            #Distance between Node1 and Node2
    CentreNode_loc=Node1+Vector*Node1Node2Dist/2                #Find location between Nodes
    return Vector,Node1Node2Dist,CentreNode_loc

def NodesDistanceCalc(Nodes):
    Nodes_dist=[]
    Nodes_dist.append(0)
    for i in range(0,len(Nodes)-1):
        Nodes_dist.append(ProximityCalculator2(Nodes[i],Nodes[i+1]))
    Max_dist=np.max(Nodes_dist)
    Nodes_dist_total=np.sum(Nodes_dist)
    return Nodes_dist,Max_dist,Nodes_dist_total

def RaypathDistCalculate(Evlon,Evlat,Evdepth,Stalon,Stalat,Distance,Azimuth):
    '''#For Now we just want to calculate the total length of the PKPdf and PKPbc raypaths for use with geometrical spreading corrections'''
    #PKPdf
    Raypath=AK135RaypathExtract(Evdepth,Distance,'PKIKP')                   #Get Raypath information from Taup
    Nodes,Nodes_sph=RayPathCartesianConvert(Raypath,Evlon,Evlat,Azimuth)    #Convert Raypath information to cartesian
    
    #Subsample
    Nodes_dist,Max_dist,Nodes_dist_total=NodesDistanceCalc(Nodes)
    Dist_tol_ratio=0.001  #The maximum Distance allowed between nodes as a fraction of total raypath length
    Dist_tol_abs=Nodes_dist_total*Dist_tol_ratio
    while Max_dist > Dist_tol_abs:
        Nodes=ResamplePathSmart(Nodes,Nodes_dist,Dist_tol_abs)
        Nodes_dist,Max_dist,Nodes_dist_total=NodesDistanceCalc(Nodes)
    PKPdf_dist=Nodes_dist_total

    #PKPdf
    Raypath=AK135RaypathExtract(Evdepth,Distance,'PKP')                     #Get Raypath information from Taup
    Nodes,Nodes_sph=RayPathCartesianConvert(Raypath,Evlon,Evlat,Azimuth)    #Convert Raypath information to cartesian
    
    #Subsample
    Nodes_dist,Max_dist,Nodes_dist_total=NodesDistanceCalc(Nodes)
    Dist_tol_ratio=0.01  #The maximum Distance allowed between nodes as a fraction of total raypath length
    Dist_tol_abs=Nodes_dist_total*Dist_tol_ratio
    while Max_dist > Dist_tol_abs:
        Nodes=ResamplePathSmart(Nodes,Nodes_dist,Dist_tol_abs)
        Nodes_dist,Max_dist,Nodes_dist_total=NodesDistanceCalc(Nodes)
    PKPbc_dist=Nodes_dist_total
    return PKPdf_dist, PKPbc_dist

def RaypathNodes(Evlon,Evlat,Evdepth,Stalon,Stalat,Distance,Azimuth):
    '''Create a Raypath Objeect'''
    #PKPdf
    Raypath=AK135RaypathExtract(Evdepth,Distance,'PKIKP')                   #Get Raypath information from Taup
    Nodes,Nodes_sph=InnerCoreRayPathCartesianConvert(Raypath,Evlon,Evlat,Azimuth)    #Convert Raypath information to cartesian
    
    #Subsample
    Nodes_dist,Max_dist,Nodes_dist_total=NodesDistanceCalc(Nodes)
    Dist_tol_ratio=0.01  #The maximum Distance allowed between nodes as a fraction of total raypath length
    Dist_tol_abs=Nodes_dist_total*Dist_tol_ratio
    while Max_dist > Dist_tol_abs:
        Nodes=ResamplePathSmart(Nodes,Nodes_dist,Dist_tol_abs)
        Nodes_dist,Max_dist,Nodes_dist_total=NodesDistanceCalc(Nodes)
    return Nodes, Nodes_dist_total

#Begin
Data=pd.read_excel()
for i in range(len(Data)):
    Nodes,Nodes_dist=RaypathNodes(Evlon,Evlat,Evdepth,Stalon,Stalat,Distance,Azimuth)
    invec=Nodes[0]
    outvec=Nodes[-1]
    RP_temp=RPObject(i,Q,ICT,Zeta,invec,outvec)
    RP_temp.Nodes=Nodes
    RP_temp.Nodes_dist=Nodes_dist
    RP_temp.Save(RPObjectsDirectory+'/'+Filename+'.obj')