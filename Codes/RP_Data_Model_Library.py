############################################################################################
#####################################   Fairhead   #########################################
##### #######################################################################################
# Henry Brett, Utrecht University, 2020, h.brett@uu.nl
# This is a Library file to contain the definition for the Raypath, Model and Data classes.
# This has been adapted from the MCMC Code I have been wrking on so there will be some unneccesary
# functionality left in

import datetime,time
import sys,os,glob,gc,copy
import numpy as np
import pickle,json
import pandas as pd
from scipy.spatial import Delaunay,Voronoi,voronoi_plot_2d
from tqdm import tqdm

#Personal Functions
from Geometry_Library import *
# import Raytrace as RT #FORTRAN!
# import Raytrace2 as RT2 #FORTRAN!

#Define Global Variables------------#
global ICDepth,Surface,ICRadius 
ICDepth=5153.5
Surface=6371
ICRadius=Surface-ICDepth

########################################################################################
# DATA AND RAYPATH CLASSES
########################################################################################
class RPObject:
    '''A class which contains the information for an individual raypath including:
        -invec-outvec
        -nodes for AK135'''
    def __init__(self,Index,Q,ICT,Zeta,invec,outvec):
        self.Index=Index                                        #Global Index number of RP
        self.Q=Q                                              #Delta t
        self.ICT=ICT                                            #Inner Core Travel Time
        self.Tstar=ICT/Q
        self.Zeta=Zeta                                          #Angle Zeta in degrees
        self.Zeta_rad=np.radians(Zeta)                          #Angle Zeta in Radians
        self.Invec=invec                                        #Vector in Cartesian Co-ordinates of where RP enters IC
        self.Outvec=outvec                                      #Vector in Cartesian Co-ordinates of where RP leaves IC
        self.RPDirectionVector=DirectionVector(invec,outvec)    #DirectionVector going from Invec to Outvec          
    
    def Save(self,filename):
        with open(filename, 'wb') as file:
            pickle.dump(self, file)

class DataClass:
    '''A class to contain all aspects of data. the pandas arrays, type and D_matrix '''
    def __init__(self,D_matrix,Data_type,D_len,RPObjects):
        self.D_matrix=D_matrix      #Data Matrix
        self.Data_type=Data_type    #List of Data Type, 'bc' 'ab' or 'cd'
        self.D_len=D_len            #Length of Data Matrix
        self.RPObjects=RPObjects    #A list of RayPath Objects
        Zeta=[]
        ICT=[]
        for RP in RPObjects:
            Zeta.append(RP.Zeta)
            ICT.append(RP.ICT)
        self.Zeta=Zeta
        self.ICT=ICT

    def Save(self,filename):
        with open(filename, 'wb') as file:
            pickle.dump(self, file)

########################################################################################
# MODEL
########################################################################################
class ModelClass:
    '''This is a super important class. Indeed this drives the code: 
        1. Basis-function functions: applying the geometry and parameterization of the model space
        2. Raytracing functions: given data how much does each raypath travel through each sub volume (NON-TRIVIAL)
        3. Perterbation functions: given a perterbation from inital state reparameterize, 
            re-raytrace where applicable and spit out a new A-matrix and M-matrix.

            This is version 7.1, this is a new paradigm: we want this class to be as memory low as possible
            for purposes of storage and Eejit.'''
    def __init__(self,iteration,VolumeLocs,VolumeQ):
        #######################################################
        # BOOK KEEPING: Iteration number, Model number, Time
        #######################################################
        self.iteration=iteration        #Iteration number
        self.Time=datetime.datetime.now()
        
        #######################################################
        # VOLUME INFORMATION AND IMPLEMENTATION
        #######################################################
        #Voronoi Cell Implementation:
        self.VolumeLocs=VolumeLocs          #The location of each volume's node
        self.VolumeQ=VolumeQ                #The Q of each volume
        self.Vol_len=len(self.VolumeLocs)   #The number of volumes
        self.VolumeAdj=[]                   #The adjacency information of every volume
        for i in range(0,self.Vol_len):
            self.VolumeAdj.append([])
        #Necessary for Voronoi Tesselation: create bounding box of points.
        self.Dummy_indexes=[0,1,2,3,4,5,6,7]
        self.points=[]
        self.points.append([-3000,-3000,-3000])
        self.points.append([-3000,-3000,3000])
        self.points.append([-3000,3000,-3000])
        self.points.append([-3000,3000,3000])

        self.points.append([3000,-3000,-3000])
        self.points.append([3000,-3000,3000])
        self.points.append([3000,3000,-3000])
        self.points.append([3000,3000,3000])
        for VolLoc in self.VolumeLocs:
            self.points.append(VolLoc)
         
        #######################################################
        # RAYTRACE INFORMATION 
        #######################################################
        #Find Adjacency Information
        self.FindAdjacent(Voronoi(self.points,qhull_options='Qv'))
        self.VolumesEffected=list(range(0,self.Vol_len))
        
        #######################################################
        # M MATRIX IMPLEMENTATION
        #######################################################
        #Matrix Implementation
        self.M_len=self.Vol_len           #The length of the Model parameter matrix
        self.M_matrix=np.zeros((self.M_len))        
        for i in range(0,self.Vol_len):
            self.M_matrix[i]=self.VolumeQ[i]   #Fill Q information
     
    #######################################################
    # VORCELL
    #######################################################
    def FindAdjacent(self,vor):
        for ridge_points in vor.ridge_points:
            if not any(item in ridge_points for item in self.Dummy_indexes): 
                self.VolumeAdj[ridge_points[0]-8].append(ridge_points[1]-8)
                self.VolumeAdj[ridge_points[1]-8].append(ridge_points[0]-8)
        #Create Points Adjacent Matrix for raytracer
        self.points_adjacent=np.ones(shape=(self.Vol_len,self.Vol_len))*-1
        for i in range(0,self.Vol_len):
            for j in range(0,len(self.VolumeAdj[i])):
                self.points_adjacent[i][j]=self.VolumeAdj[i][j]

    #######################################################
    # RAYTRACING
    #######################################################
    #After a lot of pain I've decided to keep this simple. 
    #There are always to make it faster, but ultimately shaving off 0.1s per iteration is not worth the added confusion.
    #benchmark: if it can do 5000 raypaths with 7 volumes in under 0.5s I am satisfied.
    def Aparameterize(self,Raypath):
        '''For every node in a raypath find which volume it belongs to and return an array with the Aparam values'''
        return RT2.raytrace_initialguess(nodes=np.array(Raypath.Nodes,order='F'),
                points=np.array(self.points[8:],order='F'),
                points_adjacent=np.array(self.points_adjacent,order='F',dtype=np.int32),
                nodes_dist=np.array(Raypath.Nodes_dist_norm,order='F'))[1]

    def Raytrace(self,Data,G_Rp,RPEffected):
        '''With no prior information trace raypaths through model space and return A_matrix_param'''
        for RP in RPEffected:
            G_Rp[RP,:]=self.Aparameterize(Data.RPObjects[RP]) 
        ###CHECK
        if not np.allclose(G_Rp[:,::3].sum(axis=1),np.ones((Data.D_len)),atol=10**-2):
            print('Parameterization error')
            sys.exit()
        return G_Rp
    
    def WhichVorCell(self,Locs):
        return RT.raytrace(nodes=np.array(Locs,order='F'),points=np.array(self.VolumeLocs,order='F'))
    
    #######################################################
    # DATA PREDICTION + INVERSION
    #######################################################
    def DataPredict(self,G_matrix,Data):
        '''Only save the sum of the square of the difference'''
        self.Pred_min_Data_square=(np.matmul(G_matrix,self.M_matrix)/Data.ICT-Data.D_matrix_norm)**2

    def DataPredict2(self,Data):
        '''return the actually predicted data'''
        G_Az=np.zeros(shape=(Data.D_len,self.M_len))
        G_Az=Data.G_Az_maker(self,G_Az)
        G_Rp=np.zeros(shape=(Data.D_len,self.M_len))
        G_Rp=self.Raytrace(Data,G_Rp,range(0,Data.D_len))
        G_matrix=np.multiply(G_Az,G_Rp)
        self.D_pred_norm=np.matmul(G_matrix,self.M_matrix)/Data.ICT
    
    def DLS(self,Data):
        '''Run DLS on a model state and pick the best model per misfit'''
        G_Az=np.zeros(shape=(Data.D_len,self.M_len))
        G_Az=Data.G_Az_maker(self,G_Az)
        G_Rp=np.zeros(shape=(Data.D_len,self.M_len))
        G_Rp=self.Raytrace(Data,G_Rp,range(0,Data.D_len))
        G_matrix=np.multiply(G_Az,G_Rp)
        G_t=G_matrix.transpose()
     
        #DLS:
        Damping_ratios=np.logspace(0,10,num=100)
        Misfit_store=[]
        Ind_model_param_store=[]
        Model_size_store=[]
        M_params_store=[]
        for DR in Damping_ratios:
            Damping_ratio=np.identity(self.M_len)*DR
            M_0=np.zeros(self.M_len)
            Inverse_operator=np.linalg.inv(np.matmul(G_t,G_matrix)+Damping_ratio)
            M_new=np.matmul(np.matmul(Inverse_operator,G_t),Data.D_matrix)
            M_params_store.append(M_new)
            #Resolution Matrix
            Resolution=np.matmul(np.matmul(Inverse_operator,G_t),G_matrix)
            Ind_model_param=np.trace(Resolution)
            Ind_model_param_store.append(Ind_model_param)
            #Model Size
            Model_size=np.sum(M_new**2)
            Model_size_store.append(Model_size)
            #Misfit
            dt_pred=np.matmul(G_matrix,M_new)/Data.ICT
            Misfit=sqrt(np.sum((Data.D_matrix_norm-dt_pred)**2))
            Misfit_store.append(Misfit)
        return Misfit_store,Damping_ratios,Ind_model_param_store,Model_size_store,M_params_store
    
    def DLS_single(self,Data,DR):
        '''Run DLS on a model state and pick the best model per misfit'''
        G_Az=np.zeros(shape=(Data.D_len,self.M_len))
        G_Az=Data.G_Az_maker(self,G_Az)
        G_Rp=np.zeros(shape=(Data.D_len,self.M_len))
        G_Rp=self.Raytrace(Data,G_Rp,range(0,Data.D_len))
        G_matrix=np.multiply(G_Az,G_Rp)
        G_t=G_matrix.transpose()
     
        Damping_ratio=np.identity(self.M_len)*DR
        M_0=np.zeros(self.M_len)
        Inverse_operator=np.linalg.inv(np.matmul(G_t,G_matrix)+Damping_ratio)
        M_new=np.matmul(np.matmul(Inverse_operator,G_t),Data.D_matrix)
        #Resolution Matrix
        Resolution=np.matmul(np.matmul(Inverse_operator,G_t),G_matrix)
        Ind_model_param=np.trace(Resolution)
        #Model Size
        Model_size=np.sum(M_new**2)
        #Misfit
        dt_pred=np.matmul(G_matrix,M_new)/Data.ICT
        Misfit=sqrt(np.sum((Data.D_matrix_norm-dt_pred)**2))
        return Misfit,M_new,dt_pred

    def DLS_unpack(self,M_matrix):
        '''Given a M_matrix find correct Anisotropy and Isotropy and apply to the VolumeAni and VolumeIso lists'''
        self.M_matrix=M_matrix
        for i in range(0,self.Vol_len):
            self.VolumeIso[i]=self.M_matrix[i*3]
            self.VolumeAni[i]=self.M_matrix[i*3+1]+self.M_matrix[i*3+2]
        self.AniMinMax=[np.min(self.VolumeAni),np.max(self.VolumeAni)]
        self.IsoMinMax=[np.min(self.VolumeIso),np.max(self.VolumeIso)]
        
    #######################################################
    # MISCELLANEOUS FUNCTIONS
    ####################################################### 
    def RPEffectedFinder(self,G_Rp,D_len):
        '''Find which RP's need to be re-traced'''
        if len(self.VolumesEffected)==self.Vol_len:
            RPEffected=range(0,D_len)
        else:
            if self.Change_type !='Death':
                self.VolumesEffected2=np.array(self.VolumesEffected)*3
            else:
                self.VolumesEffected2=np.array(self.VolumesEffectedDeath2)*3
            RPEffected=[]
            for i in range(0,D_len):
                if any(G_Rp[i,self.VolumesEffected2] != 0):
                    RPEffected.append(i)
        return RPEffected
   
    def RandomPointInSphere(self,Radius):
        Distance=Radius*np.random.uniform()**(1/3)
        x=np.random.normal(loc=0,scale=1)
        y=np.random.normal(loc=0,scale=1)
        z=np.random.normal(loc=0,scale=1)
        return np.array([x,y,z])*Distance/(sqrt(x**2+y**2+z**2))
    
    def Copy(self,i):
        #here is the problem: deep copy is a direct I/O operation and eejit ain't fast with that.
        Model_copy=copy.deepcopy(self)
        Model_copy.number+=1
        Model_copy.iteration=i
        Model_copy.Time=datetime.datetime.now()
        return Model_copy
    
    def Save(self,filename):
        ModelObject=[self]
        with open(filename, 'wb') as file:
            pickle.dump(ModelObject, file)
    
    def TextSave(self,filename):
        with open(filename+'.json', 'w') as fp:
            json.dump(self.__dict__, fp,sort_keys=True, indent=4,default=convert)

        
########################################################################################
# MISCELLANEOUS FUNCTIONS
########################################################################################
def DataReaderRPObjects(phase_bc,phase_ab,phase_cd,phase_df,SSI,RadCondition,AK135,LLNL,df_Shift):
    '''Function to read in bc,ab and cd differential travel times, all params are true false statements'''
    CodesDirectory=sys.path[0]
    TransFolder=os.path.dirname(CodesDirectory)
    DataDirectory=TransFolder+'/Data'
    RPDirectory=DataDirectory+'/RaypathObjects'
    
    ###USE AK135 Raypaths
    if AK135:
        #ab & bc    
        if phase_ab or phase_bc:
            filename_geom=DataDirectory+"/ab_bc_df_mc.xlsx"
            DataAll  = pd.read_excel(filename_geom, sheet_name="AllData")
            #DataAll=DataAll.loc[Surface-DataAll.TPDepth >RadCondition]     #Depth Constraint only looking at MIC
            if not SSI:
                DataAll=DataAll.loc[DataAll.Region !='SSI']     #Remove SSI

        if phase_bc:    #Fetch bc Data
            Data_bc=DataAll.loc[ (DataAll.PKPbc !="DataNotGood") & (DataAll['QualityNumber'] == 1)]    #& (DataAll.Zeta > 80)
        else:
            Data_bc=[]
        
        if phase_ab :   #Fetch ab Data
            Data_ab=DataAll.loc[(DataAll.PKPab != "DataNotGood") & (DataAll['QualityNumber'] == 1)]    #& (DataAll.Zeta > 80)
            if phase_bc:    #If ab and bc data - then only take ab measurements which have no bc
                common = Data_ab.merge(Data_bc)
                Data_ab=Data_ab[(~Data_ab.Foldername.isin(common.Foldername))&(~Data_ab.Foldername.isin(common.Foldername))]
        else:
            Data_ab=[]
        
        #cd
        if phase_cd:    #Fetch cd Data
            filename_lauren=DataDirectory+"/2019_5_20_Lauren_cd_Data_radialAngleEdit.xlsx"
            Data_cd  = pd.read_excel(filename_lauren, sheet_name="AllData") # can also index sheet by name or fetch all sheets    
        else:
            Data_cd = []

        #df
        if phase_df:
            filename=DataDirectory+"/df_karen_postprocess_mc.xlsx"
            Data_df  = pd.read_excel(filename, sheet_name="Sheet1")
            if not SSI:
                Data_df=Data_df.loc[Data_df.Region !='SSI']     #Remove SSI
            #Data_df=Data_df.loc[Surface-Data_df.TPDepth < 600]     #Depth Constraint only looking at MIC
            #Data_df=Data_df.loc[Data_df.Zeta < 35]     # Only use Karen's Deep and low zeta data.... can I justify that?
        else:
            Data_df = []
        #Collect File names:
        RPFiles=[]
        for i in range(0,len(Data_bc)):
            Foldername=Data_bc["Foldername"].values[i]
            Foldername=Foldername.replace(" ","_")
            Foldername=Foldername.replace(":","_")
            RPFiles.append(RPDirectory+'/'+Foldername+'.obj')

        for i in range(0,len(Data_ab)):
            Foldername=Data_ab["Foldername"].values[i]
            Foldername=Foldername.replace(" ","_")
            Foldername=Foldername.replace(":","_")
            RPFiles.append(RPDirectory+'/'+Foldername+'.obj')
        
        for i in range(0,len(Data_cd)):
            Foldername=Data_cd["Filename"].values[i]
            RPFiles.append(RPDirectory+'CD/'+Foldername+'.obj')
        
        for i in range(0,len(Data_df)):
            Foldername=Data_df["Filename"].values[i]
            RPFiles.append(RPDirectory+'DF/'+Foldername+'.obj')

    ###USE LLNL Raypaths
    if LLNL:
        #bc
        if phase_bc:    #Fetch bc Data
            Data_bc=pd.read_excel(DataDirectory+"/PKPbc_llnl.xlsx", sheet_name="Sheet1")
            Data_bc=Data_bc.loc[(Data_bc.dt_t_bc_llnl !="llnlNotGood")]
            if not SSI:
                Data_bc=Data_bc.loc[Data_bc.Region !='SSI']     #Remove SSI
        else:
            Data_bc=[]
        
        #ab
        if phase_ab :   #Fetch ab Data
            Data_ab=pd.read_excel(DataDirectory+"/PKPab_llnl.xlsx", sheet_name="Sheet1")
            # Data_ab=Data_ab.loc[(Data_ab.dt_t_ab_llnl !="llnlNotGood")]
            if not SSI:
                Data_ab=Data_ab.loc[Data_ab.Region !='SSI']     #Remove SSI
        else:
            Data_ab=[]
        
        #cd
        if phase_cd:    #Fetch cd Data
            Data_cd=pd.read_excel(DataDirectory+"/PKPcd_llnl.xlsx", sheet_name="Sheet1")
            Data_cd=Data_cd.loc[(Data_cd.dt_t_cd_llnl !="llnlNotGood")]
            Data_cd=Data_cd.loc[Data_cd.Distance > 134]
            if not SSI:
                Data_cd=Data_cd.loc[Data_cd.Region !='SSI']     #Remove SSI
        else:
            Data_cd = []

        #df
        if phase_df:
            Data_df  = pd.read_excel(DataDirectory+'/PKPdf_llnl.xlsx', sheet_name="Sheet1")
            # Data_df=Data_df.loc[Data_df.dt_t_df_llnl != 'llnlNotGood']
            if not SSI:
                Data_df=Data_df.loc[Data_df.Region !='SSI']     #Remove SSI
        else:
            Data_df = []
        
        #Collect File names:
        RPFiles=[]    
        Data_type=[]
        for i in range(0,len(Data_bc)):
            if os.path.isfile(DataDirectory+'/LLNL_RaypathObjects_bc/'+Data_bc["Foldername"].values[i]+'.obj'):
                RPFiles.append(DataDirectory+'/LLNL_RaypathObjects_bc/'+Data_bc["Foldername"].values[i]+'.obj')
            else:
                RPFiles.append(DataDirectory+'/LLNL_RaypathObjects_1D/'+Data_bc["Foldername"].values[i]+'.obj')
            Data_type.append('bc')

        for i in range(0,len(Data_ab)):
            if os.path.isfile(DataDirectory+'/LLNL_RaypathObjects_ab/'+Data_ab["Foldername"].values[i]+'.obj'):
                RPFiles.append(DataDirectory+'/LLNL_RaypathObjects_ab/'+Data_ab["Foldername"].values[i]+'.obj')
            else:
                RPFiles.append(DataDirectory+'/LLNL_RaypathObjects_ab_1D/'+Data_ab["Foldername"].values[i]+'.obj')
            Data_type.append('ab')

        for i in range(0,len(Data_cd)):
            RPFiles.append(DataDirectory+'/LLNL_RaypathObjects_cd/'+Data_cd["Foldername"].values[i]+'.obj')
            Data_type.append('cd')

        for i in range(0,len(Data_df)):
            if os.path.isfile(DataDirectory+'/LLNL_RaypathObjectsdf/'+Data_df["Foldername"].values[i]+'.obj'):
                RPFiles.append(DataDirectory+'/LLNL_RaypathObjects_df/'+Data_df["Foldername"].values[i]+'.obj')
            else:
                RPFiles.append(DataDirectory+'/LLNL_RaypathObjects_df_1D/'+Data_df["Foldername"].values[i]+'.obj')
            Data_type.append('df')
    D_matrix=[]
    ICT=[]
    RPobjects=[]
    Zeta=[]
    D_len=len(RPFiles)
    for j in range(0,D_len): #For Every Raypath calculate its distance through the volumes it travels through and then assemble into A
        with open(RPFiles[j],'rb') as file:
            RP_temp=pickle.load(file)
        RP_temp.Index=j
        RPobjects.append(RP_temp)
        if Data_type[j]=='df' and df_Shift:
            RP_temp.dt=RP_temp.dt+1.444 #df data from Karen shows an average shift which causes a serious dissparity with the other diff. travel times.
        D_matrix.append(RP_temp.dt)
        ICT.append(RP_temp.ICT)
        Zeta.append(RP_temp.Zeta)
        #Data_type.append(RP_temp.RefPhase)
        
    D_matrix=np.array(D_matrix)
    #Implement Data Class:
    Data=DataClass(D_matrix,Data_type,D_len,RPobjects)
    Data.ICT=np.array(ICT)
    Data.D_matrix_norm=D_matrix/np.array(ICT)
    Data.Zeta=np.array(Zeta)
    return Data

def DataSynthetic(Data,Tesselation):
    VolumeLocs,VolumeAzi=ModelTesselation(Tesselation)
    if Tesselation==1:
        #Hemispheres
        #W: 4%-0%, E: 0.5%,1%
        VolumeAni=[0.04,0.005]
        VolumeIso=[0,0.01]
    if Tesselation==2:
        #Hemispheres with IIC
        VolumeAni=[0.04,0.02,0]
        VolumeIso=[0,0,0.01]
    if Tesselation==3:
        VolumeAni=[0.04,0.05,0.03,0.01,0.03,0.02,0.02]
        VolumeIso=[0,-0.01,0,0.01,0.01,0,0]
    if Tesselation==4:
        VolumeAni=[0.02,0.02,0.02,0.02,0.02,0.02,0.02] 
        VolumeIso=[0,0,0,0,0,0,0] 
    #M-MATRIX MAKE
    Model_synthetic=ModelClass(
            number=0,iteration=0,Ani_SD=0.005,Iso_SD=0.0025,Ani_prior_sd=0.06,Iso_prior_sd=0.06,Ani_prior_mean=0,Iso_prior_mean=0,
            MaxVol=100,MinVol=1,MoveSD=100,
            VolumeLocs=VolumeLocs,VolumeAni=VolumeAni,VolumeIso=VolumeIso,VolumeAzi=VolumeAzi)
    RPEffected=list(range(0,Data.D_len))
    G_Rp_syn=np.zeros(shape=(Data.D_len,Model_synthetic.M_len)) 
    G_Rp_syn=Model_synthetic.Raytrace(Data,G_Rp_syn,RPEffected)
    
    #A MATRIX STENCIL MAKE
    G_Az_syn=np.zeros(shape=(Data.D_len,Model_synthetic.M_len)) 
    G_Az_syn=Data.G_Az_maker(Model_synthetic,G_Az_syn)
    #A MATRIX FINAL MAKE
    G_matrix_syn=np.multiply(G_Rp_syn,G_Az_syn)           #Combine Stencil and Param to make final Amatrix
    Model_synthetic.Change_type='Synthetic'
    Data.D_matrix=np.matmul(G_matrix_syn,Model_synthetic.M_matrix)     #Generate synthetic data
    Data.D_matrix_norm=Data.D_matrix/Data.ICT 
    Data_SD=0.005
    #add gaussian noise
    for i in range(0,Data.D_len):
        Data.D_matrix_norm[i]=np.random.normal(loc=Data.D_matrix_norm[i],scale=Data_SD) #SD 0.005 
    return Data,Model_synthetic,Data_SD

def ModelTesselation(Tesselation_type):
    VolumeLocs=[]
    if Tesselation_type==0:  #Random Tesselation
        NPoints=30+int(np.random.rand()*30)
        FastAzimuths=[]
        for i in range(0,NPoints):
            VolumeLocs.append(RandomPointInSphere(ICRadius))
            FastAzimuths.append([0,0,ICRadius])
    else:
        if Tesselation_type== 1: 
            #SIMPLE HEMISPHERES:
            lons=[-90,90]
            lats=[0,0]
            rads=[ICRadius,ICRadius]
            FastAzimuths=[[0,0,ICRadius],[0,0,ICRadius]]
        if Tesselation_type == 2: 
            #HARRY POTTER MODEL:
            lons=[-90,0,90]
            lats=[0,0,0]
            rads=[ICRadius,0,ICRadius]
            FastAzimuths=[[0,0,ICRadius],[0,0,ICRadius],[0,0,ICRadius]]
        if Tesselation_type== 3:
            #CUBED SPHERE:
            lons=[0,-90,0,90,180,0,0]
            lats=[0,0,0,0,0,90,-90]
            rads=[0,ICRadius,ICRadius,ICRadius,ICRadius,ICRadius,ICRadius]
            FastAzimuths=[[0,0,ICRadius],[0,0,ICRadius],[0,0,ICRadius],[0,0,ICRadius],[0,0,ICRadius],[0,0,ICRadius],[0,0,ICRadius]]
        if Tesselation_type== 4:
            #CUBED SPHERE WITH WEIRD FAST AZIMUTHS:
            lons=[0,-90,0,90,180,0,0]
            lats=[0,0,0,0,0,90,-90]
            rads=[0,ICRadius,ICRadius,ICRadius,ICRadius,ICRadius,ICRadius]
            x,y,z=SphericalToCart2(90,0,ICRadius)
            FastAzimuth1=[x,y,z]
            x,y,z=SphericalToCart2(180,0,ICRadius)
            FastAzimuth2=[x,y,z]
            x,y,z=SphericalToCart2(-90,0,ICRadius)
            FastAzimuth3=[x,y,z]
            x,y,z=SphericalToCart2(0,0,ICRadius)
            FastAzimuth4=[x,y,z]
            FastAzimuths=[[0,0,ICRadius],FastAzimuth1,FastAzimuth2,FastAzimuth3,FastAzimuth4,[0,0,ICRadius],[0,0,ICRadius]]
        if Tesselation_type== 5:
            #HIGHLY DISCRETIZED:
            #NLons=5
            #NLats=5
            #NRads=5
            #Rads=np.linspace(0,ICRadius,NRads,endpoint=True)
            #Lons=np.linspace(-180,180,NLons,endpoint=False)
            #Lats=np.linspace(-90,90,NLats,endpoint=True)
            Rads=[0,800,ICRadius]
            Lats=[-90,-45,0,45,90]
            Lons=[-135,-90,-45,0,45,90,135,180] 
            FastAzimuths=[]
            VolumLocs=[]
            VolumeLocs.append([0,0,0])  #Centre
            FastAzimuths.append([0,0,ICRadius]) 
            for Rad in Rads[1:]:
                for Lat in Lats:
                    if abs(Lat) !=90:
                        for Lon in Lons:
                            x,y,z=SphericalToCart2(Lon,Lat,Rad)
                            VolumeLocs.append([x,y,z])
                            FastAzimuths.append([0,0,ICRadius]) 
                    else:
                        x,y,z=SphericalToCart2(0,Lat,Rad)
                        VolumeLocs.append([x,y,z])
                        FastAzimuths.append([0,0,ICRadius]) 
        if Tesselation_type== 6:
            #HIGHLY DISCRETIZED:
            #NLons=5
            #NLats=5
            #NRads=5
            #Rads=np.linspace(0,ICRadius,NRads,endpoint=True)
            #Lons=np.linspace(-180,180,NLons,endpoint=False)
            #Lats=np.linspace(-90,90,NLats,endpoint=True)
            Rads=[0,800,ICRadius]
            Lats=[-60,0,60]
            Lons=[-135,-90,-45,0,45,90,135,180] 
            FastAzimuths=[]
            VolumLocs=[]
            VolumeLocs.append([0,0,0])  #Centre
            FastAzimuths.append([0,0,ICRadius]) 
            for Rad in Rads[1:]:
                for Lat in Lats:
                    if abs(Lat) !=90:
                        for Lon in Lons:
                            x,y,z=SphericalToCart2(Lon,Lat,Rad)
                            VolumeLocs.append([x,y,z])
                            FastAzimuths.append([0,0,ICRadius]) 
                    else:
                        x,y,z=SphericalToCart2(0,Lat,Rad)
                        VolumeLocs.append([x,y,z])
                        FastAzimuths.append([0,0,ICRadius]) 
        
        if Tesselation_type== 7:
            Rads=np.linspace(0,ICRadius,30,endpoint=True)
            Lats=np.linspace(-90,90,50,endpoint=True)
            Lons=np.linspace(-180,180,50,endpoint=True) 
            FastAzimuths=[]
            VolumLocs=[]
            VolumeLocs.append([0,0,0])  #Centre
            FastAzimuths.append([0,0,ICRadius]) 
            for Rad in Rads[1:]:
                for Lat in Lats:
                    if abs(Lat) !=90:
                        for Lon in Lons:
                            x,y,z=SphericalToCart2(Lon,Lat,Rad)
                            VolumeLocs.append([x,y,z])
                            FastAzimuths.append([0,0,ICRadius]) 
                    else:
                        x,y,z=SphericalToCart2(0,Lat,Rad)
                        VolumeLocs.append([x,y,z])
                        FastAzimuths.append([0,0,ICRadius]) 

        #IMPLEMENT
        if Tesselation_type not in [5,6,7]:
            for i in range(0,len(lons)):
                x,y,z=SphericalToCart2(lons[i],lats[i],rads[i])
                VolumeLocs.append([x,y,z])
    return VolumeLocs,FastAzimuths
#For JSON to handle np.int
def convert(o):
    if isinstance(o, np.int64): 
        return int(o)  

def RandomPointInSphere(Radius):
    Distance=Radius*np.random.uniform()**(1/3)
    x=np.random.normal(loc=0,scale=1)
    y=np.random.normal(loc=0,scale=1)
    z=np.random.normal(loc=0,scale=1)
    return np.array([x,y,z])*Distance/(sqrt(x**2+y**2+z**2))
