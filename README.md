# Fairhead2

This is a project to write an all python seismic tomography toolbox as a teaching tool. This project will provide the codes and file architecture to use an integration over the path methodology with Voronoi cells.

It is named Fairhead for no reason other than it is my favourite place in the world:
https://en.wikipedia.org/wiki/Fair_Head

Henry Brett 2020